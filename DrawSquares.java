import java.util.Scanner;
import java.util.List;
import java.util.Arrays;
public class DrawSquares{
    public static String getString(String cha, int len){
        // Create a method to reurn a repeated String
        StringBuilder sb = new StringBuilder(len);                                     //
        for (int i=0; i<len; i++){
           sb.append(cha);
        }
        String s = sb.toString();
        return s;
    }
    public static void main(String[] args) {
        boolean drawOneMore=true;
         while (drawOneMore){                                                      
            Scanner inputScanner= new Scanner(System.in);
            int intOutRows=0;
            int intOutCols=0;
        
            List<String> list = Arrays.asList(new String[]{"rows", "columns"});
            //for loop for rows and columns input
            for(int ii=0; ii<2 ; ii++){
                boolean isIntiger= false;
                // While loop that only accepts an intger as input
                while(!isIntiger){
                    System.out.printf("Please enter number of %s: \n",list.get(ii));
                    try {
                        switch(ii){
                            case 0:
                                intOutRows=inputScanner.nextInt();
                                isIntiger=true;
                                break;
                            case 1:
                                intOutCols=inputScanner.nextInt();
                                isIntiger=true;
                                break;
                        }
                        //promts user to input again because of an error
                    } catch (Exception e) {
                        isIntiger=false;
                        System.out.println("Error, please enter an integer");
                        inputScanner.nextLine();
                    }
                }
            }
            
            boolean isString=false;
            String charToDraw=""; // character in the square

            //while loop for input of character to draw with
            while(!isString){
                System.out.println("Please enter a character to draw squares with :");
                try{
                    
                    charToDraw=inputScanner.next();
                    if(charToDraw.length()==1){
                        isString=true;   
                    }else{
                        System.out.println("Error, Please enter a single character to draw squares with");
                        isString=false;
                    }
                 // will only acceept a singe character or else it will reprompt the user       
                } catch  (Exception e){
                    isString=false;
                    System.out.println("Error, Please enter a single character to draw squares with");
                    inputScanner.nextLine();
                }
            }

            // There are 4 different lines to print in the nested square, the variables are declared to determine where to print the lines:
            int innerRowStart=3;          
            int innerRowEnd=intOutRows-2;
            int intInCols= intOutCols-4;
            boolean innerRowMid=false;  // boolean to check that the for loop is inside the inner square
            boolean innerSquare=true; 

            // inner square is a boolean to check if the outer square is big enough to fit a smaller one inside
            if (intOutRows<7 || intOutCols<7){
                innerSquare=false;
            }else{
                innerSquare=true;
            }

            for (int rows=1; rows<intOutRows+1; rows++){
                System.out.println("");   //next line
                if(rows>innerRowStart && rows<innerRowEnd){      //check if inside the smaller square
                    innerRowMid=true;   
                }else{
                    innerRowMid=false;
                }
                if (rows==1 || rows==intOutRows){
                        System.out.print(getString(charToDraw,intOutCols));    //end lines 
                }else if((rows==innerRowStart || rows==innerRowEnd) && innerSquare){                    
                        System.out.printf("%s"+" "+"%s"+" "+"%s",charToDraw,getString(charToDraw,intInCols),charToDraw); //start line or end line of inner square
                }else if (innerRowMid && innerSquare){
                        System.out.printf("%s"+" "+"%s"+"%s"+"%s"+" "+"%s",charToDraw,charToDraw,getString(" ",intInCols-2),charToDraw,charToDraw); // wil print the mid lines in the inner square
                }else{
                        System.out.printf("%s"+" "+"%s"+" "+"%s",charToDraw,getString(" ",intOutCols-4),charToDraw);   //will print  the side lines for each row f.ex "#       #" everywhere else
                }
            }
        
    
             String yesOrNo="";
             // will prompt the user to draw one more or quit the program
            do{
                System.out.println("");
                System.out.println("Do you want to draw another square (y/n)? : ");
                yesOrNo=inputScanner.next();
            }
            while(!yesOrNo.equals("y") && !yesOrNo.equals("n"));  // it will redo the promtp until valdi input is given"
            
           if (yesOrNo.equals("y")){
                drawOneMore=true;
           }else{
                drawOneMore=false;
                inputScanner.close(); 
           }  
        }
    }
}



        
